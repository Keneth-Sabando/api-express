const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

app.get('/archivo/:filename.txt', (req, res) => {
  const filename = req.params.filename;

  const filePath = path.join(__dirname, '..', 'public', 'archivos', `${filename}.txt`);

  res.type('text/plain');
  res.sendFile(filePath, (err) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error interno del servidor');
    }
  });
});

app.listen(port, () => {
  console.log(`Fake API escuchando en http://localhost:${port}`);
});